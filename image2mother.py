#!/usr/bin/python3
import sys
import math
import PIL
#import multiprocessing as mp
#from multiprocessing import Pool
#from functools import partial
#from PIL import ImageFont
from PIL import Image
from PIL import ImageDraw

#Poder agregar los sprites faltantes
#Implementar un paso adaptativo para sprites que son mas grandes
#Promedio ponderado de colores
#CORES = 6

WHITE = (255,255,255) 
BLACK = (0,0,0)

PSHIFT = 1#Only taking into account the first 16 colors

X  = 0
Y  = 1
XY = 2

#Take a nested list and return all the internal values
def flatten(lis):
    return [value for sublist in lis for value in sublist]

#Shift a list shift places to the right
def rotate_palette(palette,offset=0,shift = PSHIFT):
    return palette[0:offset*3] + palette[offset*3+shift*3:(32-offset)*3] + palette[offset*3:offset*3+shift*3]

#Create a copy of the image and apply a palette
def apply_palette(image,palette):
    img = image.copy()
    img.putpalette(palette)
    return img

#Generate a palette rotation animation
def generate_palette_cycling(frames,palette,offset,shift=1):
    rframes = []

    plt = palette
    for i in range(len(frames)):
        rframes.append(apply_palette(frames[i],plt))
        plt = rotate_palette(plt,offset,shift)
    return rframes
    #create_gif(frames,'cycling.gif')

#Create a copy of the image and apply the scroll, the speed may be an integer
def apply_scroll(image,xspeed=1,yspeed=1):
    img = image.copy()
    width, height = img.size
    if xspeed >= 0:
        subimg1 = img.crop((0,0,width-xspeed,height))
        subimg2 = img.crop((width-xspeed,0,width,height))
        img.paste(subimg2,(0,0))
        img.paste(subimg1,(xspeed,0))
    else:
        subimg1 = img.crop((0,0,abs(xspeed),height))
        subimg2 = img.crop((abs(xspeed),0,width,height))
        img.paste(subimg2,(0,0))
        img.paste(subimg1,(width-abs(xspeed),0))

    if yspeed >= 0:
        subimg1 = img.crop((0,0,width,height-yspeed))
        subimg2 = img.crop((0,height-yspeed,width,height))
        img.paste(subimg2,(0,0))
        img.paste(subimg1,(0,yspeed))
    else:
        subimg1 = img.crop((0,0,width,abs(yspeed)))
        subimg2 = img.crop((0,abs(yspeed),width,height))
        img.paste(subimg2,(0,0))
        img.paste(subimg1,(0,height-abs(yspeed)))

    return img

#Generate a background scroll animation
def generate_background_scrolling(image,xspeed=1,yspeed=1):
    img = image.copy()
    frames = [img]
    width, height = image.size
    for w in range(int(height/yspeed)-1):
        img = apply_scroll(img,xspeed,yspeed)
        frames.append(img)
    return frames
    #create_gif(frames,'scroll.gif')

#return the values of a sin wave with size elements
def sin_waveb(a,b,c,size):
    result = []
    curr = 0
    slopes = [math.copysign(1,math.cos(math.pi*b*x + c)) for x in range(size)]
    for i in range(len(slopes)):
        curr = curr + slopes[i]
        result.append(int(1.2*curr))
    return result

#generate a list with the values of a sin wave with size elements
def sin_wave(a,b,c,size):
    return [int(a*math.sin(math.pi*b*x +c)) for x in range(size)]

#Given an image it return a list of frames where each frame was shifted horizontally according to the sin wave.
def oscillationx(image,amplitude,offset,count):
    wave = sin_wave(amplitude,0.015,offset,count)
    frames = []
    for w in wave:
        frames.append(apply_scroll(image,w,0))
    return frames

#Given an image it return a list of frames where each frame was shifted vertically according to the sin wave.
def oscillationy(image,amplitude,offset,count):
    wave = sin_wave(amplitude,0.008,offset,count)
    frames = []
    for w in wave:
        frames.append(apply_scroll(image,0,w))
    return frames
        
#Given an image it return a list of frames where each frame was shifted diagonally according to the sin wave.
def oscillationxy(image,amplitude,offset,count):
    wave = sin_wave(amplitude,0.015,offset,count)
    frames = []
    for w in wave:
        frames.append(apply_scroll(image,w,w))
    return frames

#given an image generate the oscillation frames in either x, y or xy direction
def generate_oscillation(image,direction,amplitude,framecnt):
    frames = []
    for i in range(int(framecnt/2)):
        if direction == X:
            frames.append(generate_scanline(image,oscillationx(image,amplitude,i,framecnt)))
        elif direction == Y:
            frames.append(generate_scanline(image,oscillationy(image,amplitude,i,framecnt)))
        elif direction == XY:
            frames.append(generate_scanline(image,oscillationxy(image,amplitude,i,framecnt)))
        else:
            print('Wrong direction type')            
    return frames
        #create_gif(frames,'oscillationxy.gif')

#For a list of frames it will take one row of each sequentially and return one image.
def generate_scanline(image,frames):
    width,height = image.size
    line = None
    canvas = Image.new(image.mode,(width,height),1) 
    #assert(height == len(frames))
    for l in range(height):
        line = frames[l].crop((0,l,width,l+1))
        canvas.paste(line,(0,l))
    return canvas

#For two images it will alternate lines from each group.
def interlace_images(image1,image2):
    width,height = image1.size
    line = None
    canvas = Image.new(image1.mode,(width,height),1) 
    for l in range(height):
        if l % 2 == 0:
            line = image1.crop((0,l,width,l+1))
        else:
            line = image2.crop((0,l,width,l+1))
        canvas.paste(line,(0,l))
    return canvas

#Generate a interlace animation
def generate_interlace(frames1,frames2):
    frames = []
    for f in range(min(len(frames1),len(frames2))):
        frames.append(interlace_images(frames1[f],frames2[f]))
    #create_gif(frames,'interlace.gif')
    return frames

#Takes n frames of an animation and creates a gif with them
def create_gif(frames,name,duration=100):
    frames[0].save(name,save_all=True,append_images=frames[1:],optimize=True,duration=duration,loop=0)

def combine_effects(image,palette,xspeed,yspeed):
    img = image.copy()
    frames = [img]
    width, height = image.size
    for w in range(127-1):
        img = apply_scroll(img,0,0)
        frames.append(img)
    wave = sin_wave(10,0.1,0,80)
    for i in range(len(frames)):
        frames[i] = generate_scanline(frames[i],oscillationxy(frames[i],10,i,height))
    plt = palette
    for f in range(len(frames)):
        frames[f] = apply_palette(frames[f],plt)
        plt = rotate_palette(plt,offset=1)
        print(plt[:16])
    create_gif(frames,'combined4.gif')

#Modify the palette so that the first 16 colors are repeated but reversed
def mirror_palette(palette):
    palette_reverse = flatten([(palette[i-2],palette[i-1],palette[i]) for i in range(16*3-1,0,-3)])
    return palette[:16*3]+palette_reverse+palette[32*3:]

#Create a gif of an image with a palette cycling its colors
def palette_cycling(image,palette):
    img = image.copy()
    frames = [image]*128
    frames = generate_palette_cycling(frames,palette,2)
    create_gif(frames,'combined1.gif')

#Create a gif of an image with a palette cycling its colors while scrolling
def background_scrolling(image,palette):
    img = image.copy()
    #frames = [image]*128 #TODO
    frames = generate_background_scrolling(image,1,1)
    frames = generate_palette_cycling(frames,palette,2)
    create_gif(frames,'combined2.gif')

#Create a gif of an image with a palette cycling its colors while oscillating horizontally
def horizontal_oscillation(image,palette):
    img = image.copy()
    frames = [image]*128
    frames = generate_oscillation(img,X,10,256)
    frames = generate_palette_cycling(frames,palette,1)
    create_gif(frames,'combined3.gif')

#Create a gif of an image with a palette cycling its colors while interleaving the image with itself
def interleaved_oscillation(image,palette):
    img = image.copy()
    frames1 = generate_oscillation(img,X, 10,256)
    frames2 = generate_oscillation(img,X,-10,256)
    frames = generate_interlace(frames1,frames2)
    frames = generate_palette_cycling(frames,palette,2)
    create_gif(frames,'combined4.gif',100)

#Create a gif of an image with a palette cycling its colors while oscillating vertically
def vertical_oscillation(image,palette):
    img = image.copy()
    frames = [image]*128
    frames = generate_oscillation(img,Y,10,256)
    frames = generate_palette_cycling(frames,palette,1)
    create_gif(frames,'combined5.gif')

img = Image.open('294.png')
img_p = img.convert('P')
palette = img_p.getpalette()
palette = mirror_palette(palette)
palette_cycling(img,palette)
print('Finished palete_cycling')

img = Image.open('316.png')
img_p = img.convert('P')
palette = img_p.getpalette()
palette = mirror_palette(palette)
background_scrolling(img,palette)
print('Finished background_scrolling')

img = Image.open('286.png')
img_p = img.convert('P')
palette = img_p.getpalette()
palette = mirror_palette(palette)
horizontal_oscillation(img,palette)
print('Finished horizontal_oscillation')

img = Image.open('285.png')
img_p = img.convert('P')
palette = img_p.getpalette()
palette = mirror_palette(palette)
interleaved_oscillation(img,palette)
print('Finished interleaved_oscillation')

img = Image.open('196.png')
img_p = img.convert('P')
palette = img_p.getpalette()
palette = mirror_palette(palette)
vertical_oscillation(img,palette)
print('Finished vertical_oscillation')
